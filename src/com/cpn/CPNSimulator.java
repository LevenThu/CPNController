package com.cpn;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;

import org.cpntools.accesscpn.engine.highlevel.HighLevelSimulator;
import org.cpntools.accesscpn.engine.highlevel.instance.Binding;
import org.cpntools.accesscpn.engine.highlevel.instance.Instance;
import org.cpntools.accesscpn.engine.highlevel.instance.ValueAssignment;
import org.cpntools.accesscpn.engine.highlevel.utils.StreamUtillities;
import org.cpntools.accesscpn.model.Page;
import org.cpntools.accesscpn.model.PetriNet;
import org.cpntools.accesscpn.model.Place;
import org.cpntools.accesscpn.model.PlaceNode;
import org.cpntools.accesscpn.model.Transition;
import org.cpntools.accesscpn.model.importer.NetCheckException;
import org.xml.sax.SAXException;


public class CPNSimulator {
	//static
	Properties interestingTransitions=new Properties();
	Properties interestingPlaceColorsets=new Properties();
	Properties interestingServerTokens=new Properties();
	Properties interestingClientTokens=new Properties();
	Properties colorPattern=new Properties();
	Properties colorPatternMatcherGroup=new Properties();
	Pattern sendTransitionPattern=Pattern.compile("\\(\\{from=(\\d+),to=(\\d+)\\}.*");//({from=3,to=4},{id=2,mt=MUTATION,mc=(write,30,2,0,"",1),mtime=0,callback=3})
	Map<String, Pattern> patterns=new HashMap<String,Pattern>(); 


	//runtime
	HighLevelSimulator simulator=null;
	Map<String, Instance<PlaceNode>> insterestingServerPlaceInstances=new HashMap<String, Instance<PlaceNode>>();
	Map<String, Instance<PlaceNode>> insterestingClientPlaceInstances=new HashMap<String, Instance<PlaceNode>>();
	Binding binding=null;
	String time="0";
	Instance<Transition> firedTransition;

	public static void main(String[] args) throws Exception {
		String filename = "new-cassandra-net-read-write-2015-9-14.cpn";
		CPNSimulator simulator=new CPNSimulator(filename);
		simulator.modifyCPN("Cmd\nresource", "1`[]");
		simulator.printColorPattern();
		simulator.printPlaceType();
		simulator.begin();
		while(simulator.nextStep()){
			System.out.println(simulator.getCurrentStates());
			System.out.println("interesting: "+simulator.isTheTransitionInteresting());
			if(simulator.isTheTransitionInteresting()){
				int[] fromto=simulator.getFromTo();
				if(simulator.isLocal()){
					System.out.println("from "+ fromto[0] +" to "+fromto[1]);
				}else{
					System.out.println("from server "+ fromto[0] +" to server "+fromto[1]);
				}
			}
		}
		System.exit(0);
	}

	ColouredPetriNet cpn;
	PetriNet pn;
	Page page;
	public CPNSimulator(String filename) throws IOException, NetCheckException, SAXException, ParserConfigurationException{
		String modelName = null;
		if (filename != null) {
			modelName = filename.replaceAll("[.]cpn$", "").replaceAll(".*[/\\\\]", "");
		}
		if (modelName != null) {
			System.out.println("Parsing File (" + modelName + ")");
		} else {
			System.out.println("Parsing File");
		}
		FileInputStream istream=new FileInputStream(filename);
		cpn = new ColouredPetriNet(StreamUtillities.copyToString(istream), modelName);
		pn=cpn.getPetriNet();
		page=pn.getPage().get(0);
		interestingTransitions.load(new FileInputStream(new File("transitionChange.properties")));
		interestingServerTokens.load(new FileInputStream(new File("serverplacePosition.properties")));
		interestingClientTokens.load(new FileInputStream(new File("clientplacePosition.properties")));
		interestingPlaceColorsets.load(new FileInputStream(new File("placeType.properties")));
		colorPattern.load(new FileInputStream(new File("colorPattern.properties")));
		colorPatternMatcherGroup.load(new FileInputStream(new File("colorPatternMatcherGroup.properties")));
		for(Object p:colorPattern.keySet()){
			System.out.println(p+"-->"+colorPattern.getProperty(p.toString()));
			patterns.put((String)p, Pattern.compile(colorPattern.getProperty(p.toString())));
		}
	}
	public void printColorPattern(){
		System.out.println("=========colorPattern.properties============");
		for(org.cpntools.accesscpn.model.Label label :pn.getLabel()){//label is equal to HLDecllaration
			String declare=label.asString().replaceAll("\n", " ");
			if(declare.startsWith("colset")){
				String c=declare.substring(7,declare.length()-1).split("=")[0].trim();
				if(interestingPlaceColorsets.containsValue(c)){
					System.out.println("#"+declare.substring(7,declare.length()-1));
					System.out.println(c+"=");
				}
			}
		}
	}
	public void printPlaceType(){
		System.out.println("=========placeType.properties============");
		for(Place place:page.place()){
			String name=place.getName().asString().replaceAll("\n", " ");
			if(interestingServerTokens.containsKey(name)){
				System.out.println(name+"="+place.getSort().asString());
			}
			else if(interestingClientTokens.containsKey(name)){
				System.out.println(name+"="+place.getSort().asString());
			}
		}
	}

	public boolean modifyCPN(String placeName, String token){
		if(simulator!=null){
			return false;
		}else{
			for(Place place: page.readyPlaces()){
//				if(place.getName().getText().equals("Cmd\nresource")){
//					place.getInitialMarking().setText("1`[(write,10,1,1,\"\",1),(write,5,2,IntInf.toInt(time()),\"\",1)](*[(read,key,value,time,\"parameters\",level)]*)");
//					System.out.println("\t change to:"+place.getInitialMarking());
//				}
				if(place.getName().getText().equals(placeName)){
					place.getInitialMarking().setText(token);
				}
			}
			return true;
		}

	}
	public void begin() throws NetCheckException, SAXException, IOException, ParserConfigurationException, Exception{
		simulator=cpn.getSimulator();
		simulator.initialiseSimulationScheduler();
		for(Instance<PlaceNode> placenode:simulator.getAllPlaceInstances()){
			if(interestingServerTokens.containsKey(placenode.getNode().getName().asString().replaceAll("\n", " "))){
				insterestingServerPlaceInstances.put(placenode.getNode().getName().asString().replaceAll("\n", " "), placenode);
			}
			if(interestingClientTokens.containsKey(placenode.getNode().getName().asString().replaceAll("\n", " "))){
				insterestingClientPlaceInstances.put(placenode.getNode().getName().asString().replaceAll("\n", " "), placenode);
			}
		}
	}

	/** [server-[place,value]]*/
	public Map<String, Map<String, String>> getCurrentStates() throws Exception{
		Map<String, Map<String, String>> resultMap=new HashMap<>();
		@SuppressWarnings("unchecked")
		Map<String, Instance<PlaceNode>>[] intersetingPlaceInstances=new Map[]{insterestingServerPlaceInstances,insterestingClientPlaceInstances};
		for(Map<String, Instance<PlaceNode>> interesting:intersetingPlaceInstances){
			for(Map.Entry<String,Instance<PlaceNode>>place: interesting.entrySet()){
//				System.out.println(place.getKey());
				String marking=simulator.getMarking(place.getValue()).replaceAll("\n", " ");
				String[] markings=marking.split("\\+\\+");
				for(String m:markings){
					m=m.trim();
					if(!m.equals("empty")){
						Matcher matcher=patterns.get(place.getValue().getNode().getSort().asString()).matcher(m);
						//						System.out.println(m+"-->"+place.getValue().getNode().getSort().asString());
						matcher.matches();
//						System.out.println("\tserver:"+matcher.group(1)+" \t"+m);
						Map<String, String> tokensMap=resultMap.get(matcher.group(1));
						if(tokensMap==null){
							tokensMap=new HashMap<String, String>();
							resultMap.put(matcher.group(1), tokensMap);
						}
						if(tokensMap.containsKey(place.getKey())){
							tokensMap.put(place.getKey(), tokensMap.get(place.getKey())+"\n"+m);
						}else{
							tokensMap.put(place.getKey(), m);
						}

					}
				}
			}
		}
		return resultMap;
	}

	public boolean isTheTransitionInteresting(){
		return interestingTransitions.containsKey(firedTransition.getNode().getName().asString().replaceAll("\n", " "));
	}
	public boolean isLocal(){
		String transitionName=firedTransition.getNode().getName().asString().replaceAll("\n", " ");
		return !(transitionName.equals("send")||transitionName.equals("send2"));
	}
	public int[] getFromTo(){
		if(!isTheTransitionInteresting())
			return null;
		String transitionName=firedTransition.getNode().getName().asString().replaceAll("\n", " ");
		String[] type=((String)interestingTransitions.get(transitionName)).split(",");
		if(isLocal()){
			if(type[0].equals("_")){
				return new int[]{-1,Integer.valueOf(type[1])};
			}
			if(type[1].equals("_")){
				return new int[]{Integer.valueOf(type[0]),-1};
			}
			return new int[]{Integer.valueOf(type[0]),Integer.valueOf(type[1])};
		}else{
			for(ValueAssignment valueAssignment:binding.getAllAssignments()){
				Matcher matcher=sendTransitionPattern.matcher(valueAssignment.getValue().replaceAll("\n", " "));
				if(matcher.matches()){
					return new int[]{Integer.valueOf(matcher.group(1)),Integer.valueOf(matcher.group(2))};
				}
			}
		}
		return null;
	}
	public boolean nextStep() throws Exception{
		binding=simulator.executeAndGet();
		if(binding==null){
			return false;
		}else{
			time=simulator.getTime();
			firedTransition=binding.getTransitionInstance();
			System.out.println("time:"+time);
//			String transitionName=firedTransition.getNode().getName().asString().replaceAll("\n", " ");
//			System.out.println("\t fire a transition:\t"+transitionName);
		}
		return true;
	}



	public void run() throws Exception{
		while((binding=simulator.executeAndGet())!=null){
			System.out.println("time:"+simulator.getTime());
			String transitionName=binding.getTransitionInstance().getNode().getName().asString().replaceAll("\n", " ");
			System.out.println("\t fire a transition:\t"+binding.getTransitionInstance().getNode().getName().asString().replaceAll("\n", " "));
			//			for(ValueAssignment valueAssignment:binding.getAllAssignments()){
			//				System.out.println("\t\tbinding:\t"+valueAssignment.getName()+valueAssignment.getValue().replaceAll("\n", " "));
			//			}
			//			System.out.println("\t\t\tedata marking:"+simulator.getMarking(edataPlace).replaceAll("\n", " "));
			for(Map.Entry<String,Instance<PlaceNode>>place: insterestingServerPlaceInstances.entrySet()){
				//				tokens.put(place.getKey(), simulator.getMarking(place.getValue()).replaceAll("\n", " "));
				//				System.out.println(place.getKey()+":\t"+simulator.getMarking(place.getValue()).replaceAll("\n", " "));
				System.out.println(place.getKey());
				String marking=simulator.getMarking(place.getValue()).replaceAll("\n", " ");
				String[] markings=marking.split("\\+\\+");
				for(String m:markings){
					m=m.trim();
					if(!m.equals("empty")){
						Matcher matcher=patterns.get(place.getValue().getNode().getSort().asString()).matcher(m);
						//						System.out.println(m+"-->"+place.getValue().getNode().getSort().asString());
						matcher.matches();
						System.out.println("\tserver:"+matcher.group(1)+" \t"+m);
					}
				}
			}
			for(Map.Entry<String,Instance<PlaceNode>>place: insterestingClientPlaceInstances.entrySet()){
				//				tokens.put(place.getKey(), simulator.getMarking(place.getValue()).replaceAll("\n", " "));
				//				System.out.println(place.getKey()+":\t"+simulator.getMarking(place.getValue()).replaceAll("\n", " "));
				System.out.println(place.getKey());
				String marking=simulator.getMarking(place.getValue()).replaceAll("\n", " ");
				String[] markings=marking.split("\\+\\+");
				for(String m:markings){
					m=m.trim();
					if(!m.equals("empty")){
						Matcher matcher=patterns.get(place.getValue().getNode().getSort().asString()).matcher(m);
						//						System.out.println(m+"-->"+place.getValue().getNode().getSort().asString());
						matcher.matches();
						System.out.println("\tclient:"+matcher.group(2)+"\t"+m);
					}
				}
			}

			if(interestingTransitions.containsKey(transitionName)){
				System.err.println("an interesting transition:"+ transitionName);
				String[] type=((String)interestingTransitions.get(transitionName)).split(",");
				if(type[0].equals("_")){
					System.out.println("from center to "+type[1]);
				}else if(type[1].equals("_")){
					if(transitionName.equals("send")||transitionName.equals("send2")){
						for(ValueAssignment valueAssignment:binding.getAllAssignments()){
							Matcher matcher=sendTransitionPattern.matcher(valueAssignment.getValue().replaceAll("\n", " "));
							if(matcher.matches()){
								System.out.println("from"+matcher.group(1)+ "to server "+matcher.group(2));
							}
						}
					}else{
						System.out.println("from "+type[0]+" to half center ");
					}
				}else{
					System.out.println("from "+type[0] + " to "+ type[1]);
				}
			}
		}

		//		System.out.println("------------------------------");		
		//		System.out.println("===========place============");
		//		for(String place: places.keySet()){
		//			System.out.println(place+"-->"+places.get(place));
		//		}
		//		System.out.println("===========transition============");
		//		for(String transition: transitions){
		//			System.out.println(transition);
		//		}
		//		System.out.println("===========colorset============");
		//		for(String var: vars.keySet()){
		//			System.out.println(var+"-->"+vars.get(var));
		//		}		

		System.exit(0);

	}

}
