package com.cpn.experiment;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;

import org.cpntools.accesscpn.engine.highlevel.HighLevelSimulator;
import org.cpntools.accesscpn.engine.highlevel.instance.Binding;
import org.cpntools.accesscpn.engine.highlevel.utils.StreamUtillities;
import org.cpntools.accesscpn.model.PetriNet;

import com.cpn.ColouredPetriNet;
import com.cpn.memory.IMonitorService;
import com.cpn.memory.MonitorInfoBean;
import com.cpn.memory.MonitorServiceImpl;

public class TimedProtocol {

	public static void countAndWrite(HighLevelSimulator simulator, String fileName) throws Exception {
		Binding binding = null;
		int count = 0;
		//BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
		//writer.write("count,memoryUse");
		//writer.newLine();
		long start = System.currentTimeMillis();
		while ((binding = simulator.executeAndGet()) != null) {
			String transitionName = binding.getTransitionInstance().getNode().getName().asString().replaceAll("\n", " ").trim();
			//System.out.println(transitionName);
			if (transitionName.equals("Receive Ack")) {
				++count;
				//System.out.println("count : " + count);
			}
		}
		long end = System.currentTimeMillis();
		System.out.println("," + (end - start) + "," + count);
	}
	
	public static void main(String[] args) throws Exception {
		String fileName = "TimedProtocol.cpn";
		String modelName = "TimedProtocol";
		String fileContent = StreamUtillities.copyToString(new FileInputStream(fileName));

		long start = System.currentTimeMillis();
		ColouredPetriNet colouredPetriNet = new ColouredPetriNet(fileContent, modelName);
		PetriNet petriNet = colouredPetriNet.getPetriNet();
		HighLevelSimulator simulator = colouredPetriNet.getSimulator();
		simulator.initialiseSimulationScheduler();
		long end = System.currentTimeMillis();
		System.out.print((end - start));
		
		String writeTo = "TimedProtocol.csv";
		countAndWrite(simulator, writeTo);
		//Use time : 199ms, count : 13, for 8 messages.
		//Use time : 416ms, count : 31, for 16 messages.
		//Use time : 696ms, count : 53, for 32 messages.
		//Use time : 1211ms, count : 93, for 64 messages.
		//Use time : 2283ms, count : 190, for 128 messages.
		//Use time : 6398ms, count : 375, for 256 messages.
		//Use time : 23921ms, count : 769, for 512 messages.
		//Use time : 143974ms, count : 1527, for 1024messages.
		
		//generateData();
	}
	
	public static void generateData() {
		// 1`(1,&quot;Modellin&quot;)++
		for (int i = 0; i < 10240; ++i) {
			System.out.println(String.format("1`(%d,&quot;Modellin&quot;)++", (i+1)));
		}
	}
}
