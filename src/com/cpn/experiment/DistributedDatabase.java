package com.cpn.experiment;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;

import org.cpntools.accesscpn.engine.highlevel.HighLevelSimulator;
import org.cpntools.accesscpn.engine.highlevel.instance.Binding;
import org.cpntools.accesscpn.engine.highlevel.utils.StreamUtillities;
import org.cpntools.accesscpn.model.PetriNet;

import com.cpn.ColouredPetriNet;
import com.cpn.memory.IMonitorService;
import com.cpn.memory.MonitorInfoBean;
import com.cpn.memory.MonitorServiceImpl;

public class DistributedDatabase {

    public static void countAndWrite(HighLevelSimulator simulator, String fileName) throws Exception {
        Binding binding = null;
        int count = 0;
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
        writer.write("count,memoryUse");
        writer.newLine();
        while ((binding = simulator.executeAndGet()) != null) {
            String transitionName = binding.getTransitionInstance().getNode().getName().asString().replaceAll("\n", " ").trim();
            //if (transitionName.equals("Receive a Message")) {
            System.out.println(transitionName);
            ++count;
            if (count % 100 == 0) {
                IMonitorService service = new MonitorServiceImpl();
                MonitorInfoBean monitorInfo = service.getMonitorInfoBean();
                writer.write(count + "," + (monitorInfo.getTotalMemory() - monitorInfo.getFreeMemory() + 0.0) / 1024 + "MB");
                writer.newLine();
            }

            if (count == 10000) {
                System.out.println("!!!!!!!!-----------------------Done " + fileName + "-----------------------!!!!!!!!!");
                writer.close();
                return;
            }
            //}
        }
    }

    public static void main(String[] args) throws Exception {

        String fileName = "DistributedDataBase.cpn";
        String modelName = "DistributedDataBase";
        String fileContent = StreamUtillities.copyToString(new FileInputStream(fileName));

        ColouredPetriNet colouredPetriNet = new ColouredPetriNet(fileContent, modelName);
        PetriNet petriNet = colouredPetriNet.getPetriNet();
        HighLevelSimulator simulator = colouredPetriNet.getSimulator();
        simulator.initialiseSimulationScheduler();
        String writeTo = "P100I.csv";
        countAndWrite(simulator, writeTo);

    }
}
