package com.cpn;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;

import org.cpntools.accesscpn.engine.highlevel.HighLevelSimulator;
import org.cpntools.accesscpn.engine.highlevel.instance.Binding;
import org.cpntools.accesscpn.engine.highlevel.instance.Instance;
import org.cpntools.accesscpn.engine.highlevel.instance.Marking;
import org.cpntools.accesscpn.engine.highlevel.instance.ValueAssignment;
import org.cpntools.accesscpn.engine.highlevel.utils.StreamUtillities;
import org.cpntools.accesscpn.model.Arc;
import org.cpntools.accesscpn.model.FusionGroup;
import org.cpntools.accesscpn.model.HLDeclaration;
import org.cpntools.accesscpn.model.HLMarking;
import org.cpntools.accesscpn.model.HasLabel;
import org.cpntools.accesscpn.model.Page;
import org.cpntools.accesscpn.model.PetriNet;
import org.cpntools.accesscpn.model.Place;
import org.cpntools.accesscpn.model.PlaceNode;
import org.cpntools.accesscpn.model.ToolInfo;
import org.cpntools.accesscpn.model.Transition;
import org.cpntools.accesscpn.model.graphics.AnnotationGraphics;
import org.cpntools.accesscpn.model.graphics.Graphics;
import org.cpntools.accesscpn.model.importer.NetCheckException;
import org.xml.sax.SAXException;

public class TestCPN {

	public static void main(String[] args) throws NetCheckException, SAXException, IOException, ParserConfigurationException, Exception {
		List<String> colorsets=new ArrayList<String>();
		Map<String, String> vars=new HashMap<String, String>();
		Map<String,String> places=new HashMap<String,String>();//place,colorset 




		Properties interestingTransitions=new Properties();
		interestingTransitions.load(new FileInputStream(new File("transitionChange.properties")));
		Properties interestingPlaceColorsets=new Properties();
		interestingPlaceColorsets.load(new FileInputStream(new File("placeType.properties")));
		Properties interestingServerTokens=new Properties();
		interestingServerTokens.load(new FileInputStream(new File("serverplacePosition.properties")));
		Properties interestingClientTokens=new Properties();
		interestingClientTokens.load(new FileInputStream(new File("clientplacePosition.properties")));
		Properties colorPattern=new Properties();
		colorPattern.load(new FileInputStream(new File("colorPattern.properties")));
		Properties colorPatternMatcherGroup=new Properties();
		colorPatternMatcherGroup.load(new FileInputStream(new File("colorPatternMatcherGroup.properties")));
		Pattern sendTransitionPattern=Pattern.compile("\\(\\{from=(\\d+),to=(\\d+)\\}.*");//({from=3,to=4},{id=2,mt=MUTATION,mc=(write,30,2,0,"",1),mtime=0,callback=3})
		Map<String, Pattern> patterns=new HashMap<String,Pattern>(); 

		for(Object p:colorPattern.keySet()){
			System.out.println(p+"-->"+colorPattern.getProperty(p.toString()));
			patterns.put((String)p, Pattern.compile(colorPattern.getProperty(p.toString())));
		}

		ColouredPetriNet cpn;
		// Load the CPN model file.
		String modelName = null;
		String filename = "new-cassandra-net-read-write-2015-9-14.cpn";
		//		String filename=args[0];
		if (filename != null) {
			modelName = filename.replaceAll("[.]cpn$", "").replaceAll(".*[/\\\\]", "");
		}
		if (modelName != null) {
			System.out.println("Parsing File (" + modelName + ")");
		} else {
			System.out.println("Parsing File");
		}
		FileInputStream istream=new FileInputStream(filename);

		cpn = new ColouredPetriNet(StreamUtillities.copyToString(istream), modelName);

		PetriNet pn=cpn.getPetriNet();
		//		Iterator<HLDeclaration> it_declaration=pn.declaration().iterator();
		//		while(it_declaration.hasNext()){
		//			System.out.println("A declaration:\t"+it_declaration.next());
		//		}
		//		System.out.println("Page--------------------");
		//		System.out.println(pn.getId());
		//		System.out.println(pn.getKind());
		//		System.out.println(pn.getName());
		//		for(org.cpntools.accesscpn.model.Label label :pn.getLabel()){//label is equal to HLDecllaration
		//			System.out.println("A label:\t"+label.asString().replaceAll("\n", " "));
		//			String declare=label.asString().replaceAll("\n", " ");
		//			if(declare.startsWith("colset")){
		//				colorsets.add(declare.split(" ")[1]);
		//			}else if(declare.startsWith("var")){
		//				String[] dd=declare.substring(4,declare.length()-1).split(":");
		//				String colorset=dd[1].trim();
		//				dd=dd[0].split(",");
		//				for(String d:dd){
		//					vars.put(d.trim(), colorset);
		//				}
		//			}
		//		}

		// print interesting properties
		System.out.println("=========colorPattern.properties============");
		for(org.cpntools.accesscpn.model.Label label :pn.getLabel()){//label is equal to HLDecllaration
			String declare=label.asString().replaceAll("\n", " ");
			if(declare.startsWith("colset")){
				String c=declare.substring(7,declare.length()-1).split("=")[0].trim();
				if(interestingPlaceColorsets.containsValue(c)){
					System.out.println("#"+declare.substring(7,declare.length()-1));
					System.out.println(c+"=");
				}
			}
		}

		//		int i=0;
		//		Page top=pn.getPage().get(0);
		Page page=pn.getPage().get(0);
		System.out.println("\n\n\n\nPage-------------");
		//			System.out.println(page.getName().asString().replaceAll("\n", " "));
		System.out.println("Place---------------------");
		//		i=0;
		for(Place place:page.place()){
			//				System.out.println(place.getSort());
			//				System.out.println("\t"+(i++)+":"+place.getName().asString().replaceAll("\n", " "));
			//				System.out.println(place.getName().asString().replaceAll("\n", " ").replace("  ", " ").replace(" ", "\\ ")+"=");
			places.put(place.getName().asString().replaceAll("\n", " "),place.getSort().asString());
		}
		//		System.err.println(places.containsKey("read result2"));
		//			System.out.println("Transition-------------");
		//			i=0;
		//			for(Transition transition:page.transition()){
		//				System.out.println("\t"+(i++)+":"+transition.getName().asString().replace("\n", " "));
		//				transitions.add(transition.getName().asString().replace("\n", " "));

		//		System.out.println("FusionGroup--------------");
		//		for(FusionGroup fg: pn.getFusionGroups()){
		//			System.out.println(fg.getName().asString().replaceAll("\n", " "));
		//		}

		//		
		//		System.out.println("Marking place------------");
		//		i=0;
		//		for(Place place: top.readyPlaces()){
		//			System.out.println("\t"+(i++)+":"+place.getName().getText().replaceAll("\n", " ")+":\t"+place.getInitialMarking());
		//			if(place.getName().getText().equals("Cmd\nresource")){
		//				place.getInitialMarking().setText("1`[(write,10,1,1,\"\",1),(write,5,2,IntInf.toInt(time()),\"\",1)](*[(read,key,value,time,\"parameters\",level)]*)");
		//				System.out.println("\t change to:"+place.getInitialMarking());
		//			}
		//		}


		HighLevelSimulator simulator=cpn.getSimulator();
		simulator.initialiseSimulationScheduler();
		System.out.println("marking-----------------------");
		Map<String, Instance<PlaceNode>> insterestingServerPlaceInstances=new HashMap<String, Instance<PlaceNode>>();
		Map<String, Instance<PlaceNode>> insterestingClientPlaceInstances=new HashMap<String, Instance<PlaceNode>>();
		// print interesting properties	
		System.out.println("========placeType.properties================");
		for(Instance<PlaceNode> placenode:simulator.getAllPlaceInstances()){
			if(interestingServerTokens.containsKey(placenode.getNode().getName().asString().replaceAll("\n", " "))){
				insterestingServerPlaceInstances.put(placenode.getNode().getName().asString().replaceAll("\n", " "), placenode);
				System.out.println(placenode.getNode().getName().asString().replaceAll("\n", " ").replace(" ", "\\ ")+"="+placenode.getNode().getSort().asString());
			}
			if(interestingClientTokens.containsKey(placenode.getNode().getName().asString().replaceAll("\n", " "))){
				insterestingClientPlaceInstances.put(placenode.getNode().getName().asString().replaceAll("\n", " "), placenode);
				System.out.println(placenode.getNode().getName().asString().replaceAll("\n", " ").replace(" ", "\\ ")+"="+placenode.getNode().getSort().asString());
			}
		}

		System.out.println("\n\n\n\n\n=============begin to run==================");
		//		System.out.println("\t\tedata marking:"+simulator.getMarking(edataPlace).replaceAll("\n", " "));
		Binding binding=null;
		while((binding=simulator.executeAndGet())!=null){
			System.out.println("time:"+simulator.getTime());
			String transitionName=binding.getTransitionInstance().getNode().getName().asString().replaceAll("\n", " ");
			System.out.println("\t fire a transition:\t"+binding.getTransitionInstance().getNode().getName().asString().replaceAll("\n", " "));
			//			for(ValueAssignment valueAssignment:binding.getAllAssignments()){
			//				System.out.println("\t\tbinding:\t"+valueAssignment.getName()+valueAssignment.getValue().replaceAll("\n", " "));
			//			}
			//			System.out.println("\t\t\tedata marking:"+simulator.getMarking(edataPlace).replaceAll("\n", " "));
			for(Map.Entry<String,Instance<PlaceNode>>place: insterestingServerPlaceInstances.entrySet()){
				//				tokens.put(place.getKey(), simulator.getMarking(place.getValue()).replaceAll("\n", " "));
				//				System.out.println(place.getKey()+":\t"+simulator.getMarking(place.getValue()).replaceAll("\n", " "));
				System.out.println(place.getKey());
				String marking=simulator.getMarking(place.getValue()).replaceAll("\n", " ");
				String[] markings=marking.split("\\+\\+");
				for(String m:markings){
					m=m.trim();
					if(!m.equals("empty")){
						Matcher matcher=patterns.get(place.getValue().getNode().getSort().asString()).matcher(m);
						//						System.out.println(m+"-->"+place.getValue().getNode().getSort().asString());
						matcher.matches();
						System.out.println("\tserver:"+matcher.group(1)+" \t"+m);
					}
				}
			}
			for(Map.Entry<String,Instance<PlaceNode>>place: insterestingClientPlaceInstances.entrySet()){
				//				tokens.put(place.getKey(), simulator.getMarking(place.getValue()).replaceAll("\n", " "));
				//				System.out.println(place.getKey()+":\t"+simulator.getMarking(place.getValue()).replaceAll("\n", " "));
				System.out.println(place.getKey());
				String marking=simulator.getMarking(place.getValue()).replaceAll("\n", " ");
				String[] markings=marking.split("\\+\\+");
				for(String m:markings){
					m=m.trim();
					if(!m.equals("empty")){
						Matcher matcher=patterns.get(place.getValue().getNode().getSort().asString()).matcher(m);
						//						System.out.println(m+"-->"+place.getValue().getNode().getSort().asString());
						matcher.matches();
						System.out.println("\tclient:"+matcher.group(2)+"\t"+m);
					}
				}
			}

			if(interestingTransitions.containsKey(transitionName)){
				System.err.println("an interesting transition:"+ transitionName);
				String[] type=((String)interestingTransitions.get(transitionName)).split(",");
				if(type[0].equals("_")){
					System.out.println("from center to "+type[1]);
				}else if(type[1].equals("_")){
					if(transitionName.equals("send")||transitionName.equals("send2")){
						for(ValueAssignment valueAssignment:binding.getAllAssignments()){
							Matcher matcher=sendTransitionPattern.matcher(valueAssignment.getValue().replaceAll("\n", " "));
							if(matcher.matches()){
								System.out.println("from"+matcher.group(1)+ "to server "+matcher.group(2));
							}
						}
					}else{
						System.out.println("from "+type[0]+" to half center ");
					}
				}else{
					System.out.println("from "+type[0] + " to "+ type[1]);
				}
			}
		}

		//		System.out.println("------------------------------");		
		//		System.out.println("===========place============");
		//		for(String place: places.keySet()){
		//			System.out.println(place+"-->"+places.get(place));
		//		}
		//		System.out.println("===========transition============");
		//		for(String transition: transitions){
		//			System.out.println(transition);
		//		}
		//		System.out.println("===========colorset============");
		//		for(String var: vars.keySet()){
		//			System.out.println(var+"-->"+vars.get(var));
		//		}		

		System.exit(0);

	}

}
