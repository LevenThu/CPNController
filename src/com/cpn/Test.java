package com.cpn;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Test {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		// TODO Auto-generated method stub
		Properties properties=new Properties();
		properties.load(new FileInputStream("colorPattern.properties"));
		String rule=properties.getProperty("C_LMSG");
		String test="1`({from=2,to=1},[{id=1,mt=MUTATION,mc=(write,60,1,1,\"\",1),mtime=0,callback=5}])";
		Pattern p = Pattern.compile(rule);
		 Matcher m = p.matcher(test);
		 boolean b = m.matches();
		 System.out.println(m.group(1));
		 System.out.println(m.group(2));
		 System.out.println(b);
		 
		 String ER_M = "1`((3,1),{id=10,mt=REQUEST_RESPONSE,mc=(read,60,1,1,\"\",0),mtime=670,callback=13})";
		 p=Pattern.compile(properties.getProperty("ER_M"));
		 System.out.println(p.matcher(ER_M).matches());
		 
		 String ERM_I = "1`(((2,1),{id=1,mt=MUTATION,mc=(write,60,1,1,\"\",1),mtime=0,callback=0}),1)";
		 p=Pattern.compile(properties.getProperty("ERM_I"));
		 System.out.println(p.matcher(ERM_I).matches());
		 
		 String E_LEMSG = "1`(3,[])";
		 p=Pattern.compile(properties.getProperty("E_LEMSG"));
		 System.out.println(p.matcher(E_LEMSG).matches());
		 
		 String E_KVT = "1`(1,60,1,1)";
		 p=Pattern.compile(properties.getProperty("E_KVT"));
		 System.out.println(p.matcher(E_KVT).matches());
		
		 String ERM_I_LMSG = "1`(((5,1),{id=15,mt=READ,mc=(read,60,0,0,\"\",2),mtime=500,callback=0}),2,[],R_DIGEST,[(4,6),(5,12)])";
		 p=Pattern.compile(properties.getProperty("ERM_I_LMSG"));
		 System.out.println(p.matcher(ERM_I_LMSG).matches());
	}

}
