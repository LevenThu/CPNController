package com.cpn;


import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.cpntools.accesscpn.engine.highlevel.CheckerException;
import org.cpntools.accesscpn.engine.highlevel.HighLevelSimulator;
import org.cpntools.accesscpn.engine.highlevel.LocalCheckFailed;
import org.cpntools.accesscpn.engine.highlevel.PageSorter;
import org.cpntools.accesscpn.engine.highlevel.checker.Checker;
import org.cpntools.accesscpn.engine.highlevel.checker.ErrorInitializingSMLInterface;
import org.cpntools.accesscpn.model.FusionGroup;
import org.cpntools.accesscpn.model.HLDeclaration;
import org.cpntools.accesscpn.model.Page;
import org.cpntools.accesscpn.model.PetriNet;
import org.cpntools.accesscpn.model.Place;
import org.cpntools.accesscpn.model.RefPlace;
import org.cpntools.accesscpn.model.Transition;
import org.cpntools.accesscpn.model.importer.DOMParser;
import org.cpntools.accesscpn.model.importer.NetCheckException;
import org.xml.sax.SAXException;

public class ColouredPetriNet {
	private transient PetriNet petriNet; // pertriNet accurate 
	private transient HighLevelSimulator simulator;
	private final String modelFileContents;
	private final String modelName;
	
	public ColouredPetriNet(String modelFile, String modelName) {
		this.modelFileContents = modelFile;
		this.modelName = modelName;
	}

	public ColouredPetriNet(PetriNet petriNet) {
		this.petriNet = petriNet;
		modelFileContents = "";
		modelName = petriNet.getName().getText();
	}

	public void setPetriNet(PetriNet petriNet) {
		this.petriNet = petriNet;
	}
	
	public PetriNet getPetriNet() throws NetCheckException, SAXException, IOException, ParserConfigurationException {
		if (petriNet == null) {
				petriNet = DOMParser.parse(new ByteArrayInputStream(modelFileContents.getBytes()), modelName);
		}
		return petriNet;
	}
	
	public void setSimulator(HighLevelSimulator simulator) {
		this.simulator = simulator;
	}
	
	public HighLevelSimulator getNewSimulator() {
		try {
			simulator = checkModel(getPetriNet());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return simulator;
	}
	
	public HighLevelSimulator getSimulator() throws NetCheckException, SAXException, IOException, ParserConfigurationException, Exception {
		if (simulator == null) 
			getNewSimulator();
		else
			simulator = checkModel(getPetriNet());
		return simulator;
	}
	
	private HighLevelSimulator checkModel(PetriNet petriNet) throws Exception {
		HighLevelSimulator highLevelSimulator = HighLevelSimulator.getHighLevelSimulator();
		Checker checker = new Checker(petriNet, null, highLevelSimulator);
		System.out.println("Checking CPN model");
		int worked = 0;
		worked = localCheck(worked, checker);
		worked = checkInitializing(worked, checker);
		worked = checkDeclarations(worked, checker, petriNet);
		worked = checkPages(worked, checker, petriNet);
		worked = generateInstances(worked, checker, petriNet);
		checker.initialiseSimulationScheduler();
		try {
			worked = instantiateSMLInterface(worked, checker);
		} catch (ErrorInitializingSMLInterface e) {
			// Mask this; don't work for timed models
		}
		return highLevelSimulator;
	}

	private int instantiateSMLInterface(int worked, Checker checker)
			throws ErrorInitializingSMLInterface {
		System.out.println("Instantiating SML interface");
		checker.instantiateSMLInterface();
		worked++;
		return worked;
	}

	private int generateInstances(int worked, Checker checker, PetriNet petriNet) throws IOException {
		System.out.println("Generating instances");

		for (FusionGroup fusionGroup : petriNet.getFusionGroups()) {
			checker.generateInstanceForFusionGroup(fusionGroup.getId());
			worked++;
		}

		for (Page page : petriNet.getPage()) {
			System.out.println("Generating instances for " + page.getName().getText());

			for (RefPlace refPlace : page.readyFusionGroups()) {
				checker.generateInstanceForPlace(refPlace.getId());
				worked++;
			}
			for (RefPlace refPlace : page.readyPortPlaces()) {
				checker.generateInstanceForPlace(refPlace.getId());
				worked++;
			}
			for (Place place : page.readyPlaces()) {
				checker.generateInstanceForPlace(place.getId());
				worked++;
			}

			for (Transition transition : page.readyTransitions()) {
				checker.generateInstanceForTransition(transition.getId());
				worked++;
			}
		}

		return worked;
	}

	private int checkPages(int worked, Checker checker, PetriNet petriNet) throws IOException,
			CheckerException {
		System.out.println("Checking pages");
		PageSorter pageSorter = new PageSorter(petriNet.getPage());
		for (Page page : pageSorter) {
			System.out.println("Checking page " + page.getName().getText());
			checker.checkPage(page, pageSorter.isPrime(page));
			worked += 3;
		}
		return worked;
	}

	private int checkDeclarations(int worked, Checker checker, PetriNet petriNet) throws Exception {
		System.out.println("Checking declarations");
		for (HLDeclaration decl : petriNet.declaration()) {
			checker.checkDeclaration(decl);
			worked++;
		}
		checker.generateSerializers();
		worked++;
		return worked;
	}

	private int checkInitializing(int worked, Checker checker) throws IOException, Exception {
		System.out.println("Initializing");
		checker.checkInitializing();
		worked++;
		return worked;
	}

	private int localCheck(int worked, Checker checker) throws LocalCheckFailed {
		System.out.println("Checking names");
		//checker.localCheck();
		worked++;
		return worked;
	}

	private int iterableCount(Iterable<?> it) {
		int i = 0;
		for (@SuppressWarnings("unused")
		Object o : it) {
			i++;
		}
		return i;
	}

	private int computeWorkload(PetriNet petriNet) {
		int workload = 3;
		workload += iterableCount(petriNet.declaration());
		workload += (3 * petriNet.getPage().size());
		workload += petriNet.getFusionGroups().size();
		for (Page page : petriNet.getPage()) {
			workload += iterableCount(page.readyFusionGroups());
			workload += iterableCount(page.readyPortPlaces());
			workload += iterableCount(page.readyPlaces());
			workload += iterableCount(page.readyTransitions());
		}
		workload++;
		return workload;
	}

}