package com.cpn.memory;

import java.lang.management.ManagementFactory;
import com.sun.management.OperatingSystemMXBean;

/**
 * 
 * 获取系统信息的业务逻辑实现类.
 * 
 * @author GuoHuang
 */
@SuppressWarnings("restriction")
public class MonitorServiceImpl implements IMonitorService {
	/**
	 * 获得当前的监控对象.
	 * 
	 * @return 返回构造好的监控对象
	 * @throws Exception
	 * @author GuoHuang
	 */
	public MonitorInfoBean getMonitorInfoBean() throws Exception {
		int kb = 1024;
		// 可使用内存
		long totalMemory = Runtime.getRuntime().totalMemory() / kb;
		// 剩余内存
		long freeMemory = Runtime.getRuntime().freeMemory() / kb;
		// 最大可使用内存
 		long maxMemory = Runtime.getRuntime().maxMemory() / kb;
		OperatingSystemMXBean osmxb = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
		// 操作系统
		String osName = System.getProperty("os.name");
		// 总的物理内存
		long totalMemorySize = osmxb.getTotalPhysicalMemorySize() / kb;
		// 剩余的物理内存
		long freePhysicalMemorySize = osmxb.getFreePhysicalMemorySize() / kb;
		// 已使用的物理内存
		long usedMemory = (osmxb.getTotalPhysicalMemorySize() - osmxb.getFreePhysicalMemorySize()) / kb;
		// 获得线程总数
		ThreadGroup parentThread;
		for (parentThread = Thread.currentThread().getThreadGroup(); parentThread.getParent() != null; parentThread = parentThread.getParent());
		int totalThread = parentThread.activeCount();
		
		// 构造返回对象
		MonitorInfoBean infoBean = new MonitorInfoBean();
		infoBean.setFreeMemory(freeMemory);
		infoBean.setFreePhysicalMemorySize(freePhysicalMemorySize);
		infoBean.setMaxMemory(maxMemory);
		infoBean.setOsName(osName);
		infoBean.setTotalMemory(totalMemory);
		infoBean.setTotalMemorySize(totalMemorySize);
		infoBean.setTotalThread(totalThread);
		infoBean.setUsedMemory(usedMemory);
		return infoBean;
	}
	
	/**
	 * 测试方法.
	 * 
	 * @param args
	 * @throws Exception
	 * @author GuoHuang
	 */
	public static void main(String[] args) throws Exception {
		IMonitorService service = new MonitorServiceImpl();
		MonitorInfoBean monitorInfo = service.getMonitorInfoBean();
		System.out.println("可使用内存=" + monitorInfo.getTotalMemory() + "KB");
		System.out.println("剩余内存=" + monitorInfo.getFreeMemory() + "KB");
		System.out.println("最大可使用内存=" + monitorInfo.getMaxMemory() + "KB");
		System.out.println("操作系统=" + monitorInfo.getOsName());
		System.out.println("总的物理内存=" + monitorInfo.getTotalMemorySize() + "KB");
		System.out.println("剩余的物理内存=" + monitorInfo.getFreeMemory() + "KB");
		System.out.println("已使用的物理内存=" + monitorInfo.getUsedMemory() + "KB");
		System.out.println("线程总数=" + monitorInfo.getTotalThread());
	}
}
